@extends('layouts.dashboard')

@section('content')
    <div class="card">
        <div class="card-body">
            <div class="card-header">
                <h2>Edit User</h2>
            </div>
            <div class="row">
                <div class="col-lg-12 margin-tb">
                    <div class="pull-left">
                    </div>
                    <div class="pull-right">
                        <a class="btn btn-primary" href="{{ route('users.index') }}"> Back</a>
                    </div>
                </div>
            </div>
        <div class="container mt-4">
            <form method="POST" action="{{ route('users.update', $user->id)}}">
                @csrf
                @method('put')
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Name:</strong>
                        <input type="text" placeholder='Name' name='name' class="form-control" value="{{$user->name}}">
                        @if($errors->has('name'))
                            <div class="error text-danger">{{ $errors->first('name') }}</div>
                        @endif
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Email:</strong>
                        <input type="email" placeholder='Email' name='email' class="form-control" value="{{$user->email}}">
                        @if($errors->has('email'))
                            <div class="error text-danger">{{ $errors->first('email') }}</div>
                        @endif
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Password:</strong>
                        <input type="password" placeholder='Password' name='password' class="form-control"></div>
                    @if($errors->has('password'))
                        <div class="error text-danger">{{ $errors->first('password') }}</div>
                    @endif
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Roles</strong>
                        @foreach($roles as $role)
                            <div class="form-group form-check">
                                <input class="form-check-input" {{ ($user->roles->contains('id', $role->id)) ? 'checked' : '' }} value="{{ $role->id }}" name="role_id[]" type="checkbox" id="{{ $role->name }}" >
                                <label for="{{ $role->name }}" class="form-check-label">{{ $role->name }}</label>
                            </div>
                        @endforeach
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                    <button type="submit" class="btn btn-primary">Update</button>
                </div>
            </form>
        </div>
</div>
@endsection

