@extends('layouts.dashboard')

@section('content')
    <div class="card">
        <div class="card-body">
            <div class="card-header">
                <h2>Edit Category</h2>
            </div>
            <div class="row">
                    <div class="col-lg-12 margin-tb">
                        <div class="pull-left">

                        </div>
                        <div class="pull-right">
                            <a class="btn btn-primary" href="{{ route('categories.index') }}"> Back</a>
                        </div>
                    </div>
                </div>
            <div class="container mt-4">
                    <form method="POST" action="{{ route('categories.update', ['id' => $category->id])}}">
                        @csrf
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>Name:</strong>
                                <input type="text" placeholder='Name' name='name' class="form-control" value="{{$category->name}}">
                                @if($errors->has('name'))
                                    <div class="error text-danger">{{ $errors->first('name') }}</div>
                                @endif
                            </div>
                            <div class="form-group">
                                <strong>Select a parent cateogory</strong>
                                <select class="form-control" name="parent_id">
                                    <option value="">None</option>
                                    @php
                                        $space = '';
                                    @endphp
                                    @if($categories)
                                        @foreach($categories as $item)
                                            <option
                                                value="{{$item->id}}" {{$item->id == $category->parent_id ? 'selected' : ''}}>{{$item->name}}</option>
                                            @if(count($item->children) > 0)
                                                @include('categories.children_update', ['children' => $item->children])
                                            @endif
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                            <button type="submit" class="btn btn-primary">Update</button>
                        </div>
                    </form>
                </div>
        </div>
    </div>
@endsection
