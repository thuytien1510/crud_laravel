@extends('layouts.dashboard')

@section('content')
    <div class="card">
        <div class="card-body">
            <div class="card-header">
                <h2>Create New Category</h2>
            </div>
            <div class="row">
                <div class="col-lg-12 margin-tb">
                    <div class="pull-right">
                        <a class="btn btn-primary" href="{{ route('categories.index') }}"> Back</a>
                    </div>
                </div>
            </div>
            <div class="container mt-4">
                <form method="POST" action="{{route('categories.store')}}">
                    @csrf
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>Category name</strong>
                            <input type="text" name="name" class="form-control" placeholder="Name">
                            @if($errors->has('name'))
                                <div class="error text-danger">{{ $errors->first('name') }}</div>
                            @endif
                        </div>
                        <div class="form-group">
                            <strong>Select parent category</strong>
                            <select class="form-control" name="parent_id">
                                <option value="">None</option>
                                @if($categories)
                                    @php
                                        $space = "";
                                    @endphp
                                    @foreach($categories as $value)
                                        <option value="{{$value->id}}">{{$value->name}}</option>
                                        @if($value->children())
                                            @include('categories.children',['children' => $value->children])
                                        @endif
                                    @endforeach
                                @endif
                            </select>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
