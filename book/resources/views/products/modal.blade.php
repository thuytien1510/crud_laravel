<div class="modal fade" id="myModal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Create</h4>
                <button type="button" id="x" class="close" data-dismiss="modal">&times;</button>
            </div>
            <!-- create -->
            <div class="modal-body">
                <span id="form_result"></span>
                <form method="POST" id="formCreate" class="form-horizontal" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group" style="text-align: left">
                        <label for="name">Name:</label>
                        <input type="text" name="name" class="form-control" placeholder="Enter name">
                        <span class="error-form text-danger"></span>
                    </div>
                    <div class="form-group" style="text-align: left">
                        <label for="text">Price:</label>
                        <input type="text" name="price" class="form-control" placeholder="Enter price">
                        <span class="error-form text-danger"></span>
                    </div>
                    <div class="form-group" style="text-align: left">
                        <label for="text">Description:</label>
                        <input type="text" name="description" class="form-control" placeholder="Enter description">
                        <span class="error-form text-danger"></span>
                    </div>
                    <div class="form-group" style="text-align: left">
                        <label for="pwd">Image:</label>
                        <input type="file" id="image" name="image" class="form-control">
                        <span id="store_image"></span>
                        <span class="error-form text-danger"></span>
                    </div>
                    <label for="pwd">Category:</label>
                    <div class="form-group">
                        <select class="form-control" name="parent_id">
                            <option value="">None</option>
                            @if($categories)
                                @php
                                    $space = "";
                                @endphp
                                @foreach($categories as $value)
                                    <option value="{{$value->id}}">{{$value->name}}</option>
                                    @if($value->children())
                                        @include('categories.children',['children' => $value->children])
                                    @endif
                                @endforeach
                            @endif
                        </select>
                    </div>
                    <div class="form-group" align="center">
                        <input type="hidden" name="action" id="action" />
                        <input type="hidden" name="hidden_id" id="hidden_id" />
                        <input type="submit" name="action_button" id="action_button" class="btn btn-warning" value="Add"/>
                    </div>
                    {{--                        <button type="submit" class="btn btn-primary js-btn-create">Submit</button>--}}
                </form>
            </div>
        </div>
    </div>
</div>
