@extends('layouts.dashboard')

@section('content')
    <div class="main-content position-relative max-height-vh-100 h-100 border-radius-lg ">
        <!-- Navbar -->
        <div class="row ">
            <div class="col-lg-12 margin-tb">
                <div class="pull-right">
                    <div class="card-tools">
                        <form action="{{ route('products.index') }}" method="GET" id="list-products">
                            <div class="row">
                                @permission('products-create')
                                <div class="col-sm ">
                                    {{--                                    <button class="btn btn-success js-modal-create" >Create New Product</button>--}}
                                    <button type="button" name="create_product" id="create_product" data-route-store = "{{ route('products.store') }}" class="btn btn-primary create_product">Create Product</button>
                                </div>
                                @endpermission
                                <div class="col-sm ">
                                    <input type="text" name="name" class="form-control float-right" placeholder="Name" value="{{ isset($_GET['name']) ? $_GET['name'] : '' }}">
                                </div>
                                <div class="col-sm ">
                                    <input type="text" name="price" class="form-control float-right" placeholder="Price" value="{{ isset($_GET['price']) ? $_GET['price'] : '' }}">
                                </div>
                                <div class="col-sm">
                                    <select name="category_name" id="category_name" class="form-control">
                                        <option value="">Select Category</option>
                                        @foreach($category as $key => $p)
                                            <option>{{$p->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-sm ">
                                    <button type="submit" class="btn btn-default">
                                        <i class="fas fa-search"></i>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid py-4">
            <div class="row">
                <div class="col-12">
                    <h1><p id="ms" class="text-success"></p></h1>
                    <div class="card my-4">

                        <div class="card-header p-0 position-relative mt-n4 mx-3 z-index-2">
                            <div class="bg-gradient-primary shadow-primary border-radius-lg pt-4 pb-3">
                                <h6 class="text-white text-capitalize ps-3">Product Management</h6>
                            </div>
                        </div>

                        <div class="card-body px-0 pb-2">
                            <div class="table-responsive p-0">
                                <table class="table align-items-center mb-0 table-bordered" id="user_table">
                                    <thead>
                                    <tr>
                                        <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">id</th>
                                        <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">name</th>
                                        <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">price</th>
                                        <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">image </th>
                                        <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">description</th>
                                        <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">category</th>
                                        <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach ($products??[] as $key => $product)
                                        <tr id="product_id_{{ $product->id }}">
                                            <td>
                                                <div class="d-flex px-2 py-1 col-sm-7">
                                                    <div class="d-flex flex-column justify-content-center">
                                                        <h6 class="mb-0 text-sm " id="idUser">{{ $product->id }}</h6>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <p class="text-xs font-weight-bold mb-0 col-sm-7">{{ $product->name }}</p>
                                            </td>
                                            <td class="align-middle text-left text-sm">
                                                <span class="badge badge-sm bg-gradient-success">{{ $product->price }}</span>
                                            </td>
                                            <td class="align-middle text-center text-sm col-sm-7">
                                                <span class="badge badge-sm "><img src="upload/products/{{$product->image}}" class="img" alt="alt text"> </span>
                                            </td>
                                            <td>
                                                <p class="text-xs font-weight-bold mb-0 text-truncate col-sm-7">{{ $product->description }}</p>
                                            </td>
                                            <td class="align-middle text-center col-sm-7">
                                                <span class="text-secondary text-xs font-weight-bold">{{$product->category[0]->name ??'' }}</span>
                                            </td>
                                            <td class="align-middle text-center col-sm-7">
                                                @permission('products-edit')
                                                <a href="javascript:void(0)" data-id="{{ $product->id }}" data-route-edit="{{ route('products.edit', $product->id) }}" data-route-update="{{ route('products.update', $product->id) }}" id="edit-product" class="btn btn-block btn-warning edit-product" style="margin-bottom: 11px">Edit</a>
                                                @endpermission
                                                @permission('products-delete')
                                                <div method="POST" >
                                                    @csrf
                                                    @method('DELETE')
                                                    <a href="javascript:void(0)" data-id="{{ $product->id }}" data-route-destroy="{{ route('products.destroy', $product->id) }}" id="delete-product" class="btn btn-block btn-danger delete-product" style="margin-bottom: 11px" onclick=" return confirm('are you sure ?')">Delete</a>
                                                </div>
                                                @endpermission
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    {{ $products->links() }}
                </div>
            </div>
        </div>
    </div>
    @include('products.modal')
    {{--    modal--}}
{{--    <div class="modal fade" id="myModal">--}}
{{--        <div class="modal-dialog modal-lg">--}}
{{--            <div class="modal-content">--}}

{{--                <!-- Modal Header -->--}}
{{--                <div class="modal-header">--}}
{{--                    <h4 class="modal-title">Create</h4>--}}
{{--                    <button type="button" id="x" class="close" data-dismiss="modal">&times;</button>--}}
{{--                </div>--}}
{{--                <!-- create -->--}}
{{--                <div class="modal-body">--}}
{{--                    <span id="form_result"></span>--}}
{{--                    <form action="" method="POST" data-route="{{ route('products.store') }}" enctype="multipart/form-data" id="formCreate">--}}
{{--                        @csrf--}}
{{--                        <div class="form-group" style="text-align: left">--}}
{{--                            <label for="name">Name:</label>--}}
{{--                            <input type="text" name="name" class="form-control" placeholder="Enter name">--}}
{{--                            <span class="error-form text-danger"></span>--}}
{{--                        </div>--}}
{{--                        <div class="form-group" style="text-align: left">--}}
{{--                            <label for="text">Price:</label>--}}
{{--                            <input type="text" name="price" class="form-control" placeholder="Enter price">--}}
{{--                            <span class="error-form text-danger"></span>--}}
{{--                        </div>--}}
{{--                        <div class="form-group" style="text-align: left">--}}
{{--                            <label for="text">Description:</label>--}}
{{--                            <input type="text" name="description" class="form-control" placeholder="Enter description">--}}
{{--                            <span class="error-form text-danger"></span>--}}
{{--                        </div>--}}
{{--                        <div class="form-group" style="text-align: left">--}}
{{--                            <label for="pwd">Image:</label>--}}
{{--                            <input type="file" id="image" name="image" class="form-control">--}}
{{--                            <span id="store_image"></span>--}}
{{--                            <span class="error-form text-danger"></span>--}}
{{--                        </div>--}}
{{--                        <label for="pwd">Category:</label>--}}
{{--                        <div class="form-group">--}}
{{--                            <select class="form-control" name="parent_id">--}}
{{--                                <option value="">None</option>--}}
{{--                                @if($categories)--}}
{{--                                    @php--}}
{{--                                        $space = "";--}}
{{--                                    @endphp--}}
{{--                                    @foreach($categories as $value)--}}
{{--                                        <option value="{{$value->id}}">{{$value->name}}</option>--}}
{{--                                        @if($value->children())--}}
{{--                                            @include('categories.children',['children' => $value->children])--}}
{{--                                        @endif--}}
{{--                                    @endforeach--}}
{{--                                @endif--}}
{{--                            </select>--}}
{{--                        </div>--}}
{{--                        <div class="form-group" align="center">--}}
{{--                            <input type="hidden" name="action" id="action" />--}}
{{--                            <input type="hidden" name="hidden_id" id="hidden_id" />--}}
{{--                            <input type="submit" name="action_button" id="action_button" class="btn btn-warning" value="Add"/>--}}
{{--                        </div>--}}
{{--                        --}}{{--                        <button type="submit" class="btn btn-primary js-btn-create">Submit</button>--}}
{{--                    </form>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}

    <!--edit -->
    {{--    <div class="modal fade" id="myModalUpdate">--}}
    {{--        <div class="modal-dialog modal-lg">--}}
    {{--            <div class="modal-content">--}}
    {{--                <div class="modal-header">--}}
    {{--                    <h4 class="modal-title">Edit</h4>--}}
    {{--                    <button type="button" id="x" class="close" data-dismiss="modal">&times;</button>--}}
    {{--                </div>--}}
    {{--                <div class="modal-body">--}}
    {{--                    @foreach ($products??'' as $key => $d)--}}
    {{--                        <form method="POST" data-route="{{route('products.update',['id'=>$d->id])}}" id="form-update">--}}
    {{--                            @endforeach--}}
    {{--                            @csrf--}}
    {{--                            <input type="hidden" id="id" name="id" class="form-control">--}}
    {{--                            <div class="form-group" style="text-align: left">--}}
    {{--                                <label for="text">Name:</label>--}}
    {{--                                <input type="text" name="name" id="name"  class="form-control" placeholder="Enter name">--}}
    {{--                                <span class="error-form text-danger"></span>--}}
    {{--                            </div>--}}
    {{--                            <div class="form-group" style="text-align: left">--}}
    {{--                                <label for="text">Price:</label>--}}
    {{--                                <input type="text" name="price" id="price"  class="form-control" placeholder="Enter price">--}}
    {{--                                <span class="error-form text-danger"></span>--}}
    {{--                            </div>--}}
    {{--                            <div class="form-group" style="text-align: left">--}}
    {{--                                <label for="text">Description:</label>--}}
    {{--                                <input type="text" name="description" id="description"  class="form-control" placeholder="Enter description">--}}
    {{--                                <span class="error-form text-danger"></span>--}}
    {{--                            </div>--}}
    {{--                            <div class="form-group" style="text-align: left">--}}
    {{--                                <label for="pwd">Image:</label>--}}
    {{--                                <input type="file" id="image" name="image"  class="form-control">--}}
    {{--                                <span class="error-form text-danger"></span>--}}
    {{--                                <span id="store_image"></span>--}}
    {{--                            </div>--}}
    {{--                            <label for="pwd">Category:</label>--}}
    {{--                            <select class="form-control" name="parent_id">--}}
    {{--                                <option value="">None</option>--}}
    {{--                                @if($categories)--}}
    {{--                                    @php--}}
    {{--                                        $space = "";--}}
    {{--                                    @endphp--}}
    {{--                                    @foreach($categories as $value)--}}
    {{--                                        <option value="{{$value->id}}">{{$value->name}}</option>--}}
    {{--                                        @if($value->children())--}}
    {{--                                            @include('categories.children',['children' => $value->children])--}}
    {{--                                        @endif--}}
    {{--                                    @endforeach--}}
    {{--                                @endif--}}
    {{--                            </select>--}}
    {{--                            <button type="submit" class="btn btn-primary js-btn-update">Submit</button>--}}
    {{--                        </form>--}}
    {{--                </div>--}}
    {{--            </div>--}}
    {{--        </div>--}}
    {{--    </div>--}}
    {{--    <!-- delete -->--}}
    {{--    <div class="modal" method="POST" id="myModalDelete" tabindex="-1" role="dialog">--}}
    {{--        @csrf--}}
    {{--        @method('post')--}}
    {{--        <div class="modal-dialog" role="document">--}}
    {{--            <div class="modal-content">--}}
    {{--                <div class="modal-header">--}}
    {{--                    <h5 class="modal-title">Delete</h5>--}}
    {{--                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">--}}
    {{--                        <span aria-hidden="true">&times;</span>--}}
    {{--                    </button>--}}
    {{--                </div>--}}
    {{--                <div class="modal-body">--}}
    {{--                    <p> Are you sure?</p>--}}
    {{--                </div>--}}
    {{--                <div class="modal-footer">--}}
    {{--                    <button type="button" class="btn btn-danger js-btn-delete">Delete</button>--}}
    {{--                    <button type="button" class="btn btn-secondary" id="xx" data-dismiss="modal">Close</button>--}}
    {{--                </div>--}}
    {{--            </div>--}}
    {{--        </div>--}}
    {{--    </div>--}}
{{--        <script src="{{asset('js/product.js')}}"></script>--}}
{{--    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>--}}
    <script src="{{ asset('/js/script.js') }}"></script>
    <script src="{{ asset('/js/module.js') }}"></script>
@endsection
