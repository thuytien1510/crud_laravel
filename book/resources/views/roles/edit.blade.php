@extends('layouts.dashboard')

@section('content')
    <div class="card">
        <div class="card-body">
            <div class="card-header">
                <h2>Edit Role</h2>
            </div>
            <div class="row">
                <div class="col-lg-12 margin-tb">
                    <div class="pull-left">
                    </div>
                    <div class="pull-right">
                        <a class="btn btn-primary" href="{{ route('roles.index') }}"> Back</a>
                    </div>
                </div>
            </div>

            <div class="container mt-4">
                <form method="POST" action="{{ route('roles.update', ['id' => $role->id])}}">
                    @csrf
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>Name:</strong>
                            <input type="text" placeholder='Name' name='name' class="form-control" value="{{$role->name}}">
                            @if($errors->has('name'))
                                <div class="error text-danger">{{ $errors->first('name') }}</div>
                            @endif
                        </div>
                        <div class="form-group">
                            <strong>Permission:</strong>
                            @if($errors->has('permission'))
                                <div class="error text-danger">{{ $errors->first('permission') }}</div>
                            @endif
                        </div>
                        @foreach($permissions as $permission)
                            <div class="form-group form-check">
                                <input class="form-check-input" {{ ($role->permissions->contains('id', $permission->id)) ? 'checked' : '' }} value="{{ $permission->id }}" name="permission[]" type="checkbox" id="{{ $permission->name }}" >
                                <label for="{{ $permission->name }}" class="form-check-label">{{ $permission->name }}</label>
                            </div>
                        @endforeach

                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                        <button type="submit" class="btn btn-primary">Update</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection


