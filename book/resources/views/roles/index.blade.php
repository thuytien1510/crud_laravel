@extends('layouts.dashboard')

@section('content')
    @if (\Session::has('message'))
        <div class="alert alert-success">
            <strong>Success:</strong>{!! \Session::get('message') !!}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @elseif (\Session::has('error'))
        <div class="alert alert-danger">
            <strong>Error:</strong>{!! \Session::get('error') !!}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif
        <div class="row">
            <div class="col-lg-12 margin-tb">
                <div class="pull-right">
                    <div class="card-tools">
                        <form action="{{ route('roles.search') }}" method="GET">
                            <div class="row">
                                @permission('role-create')
                                <div class="col-sm">
                                    <a class="btn btn-success" href="{{ route('roles.create') }}"> Create New Role</a>
                                </div>
                                @endpermission
                                <div class="col-sm ">
                                    <input type="text" name="name" class="form-control float-right " placeholder="Name" value="{{ isset($_GET['name']) ? $_GET['name'] : '' }}">
                                </div>
                                <div class="col-sm ">
                                    <button type="submit" class="btn btn-default ">
                                        <i class="fas fa-search"></i>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div class="row ">
            <div class="container-fluid py-4">
                <div class="row">
                    <div class="col-12">
                        <div class="card my-4">
                            <div class="card-header p-0 position-relative mt-n4 mx-3 z-index-2">
                                <div class="bg-gradient-primary shadow-primary border-radius-lg pt-4 pb-3">
                                    <h6 class="text-white text-capitalize ps-3">Roles Management</h6>
                                </div>
                            </div>
                            <div class="card-body px-0 pb-2">
                                <div class="table-responsive p-0">
                                    <table class="table align-items-center mb-0">
                                        <thead>
                                        <tr>
                                            <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">No</th>
                                            <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">Name</th>
                                            <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Action</th>
                                            <th class="text-secondary opacity-7"></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach ($roles as $key => $role)                                            <tr>
                                                <td>
                                                    <div class="align-middle text-center text-sm">
                                                        <h6 class="mb-0 text-sm">{{ $role->id }}</h6>
                                                    </div>
                                                </td>
                                                <td>
                                                    <p class="text-xs font-weight-bold mb-0">{{ $role->name }}</p>
                                                </td>
                                                <td class="align-middle text-center">
                                                     <a class="btn btn-info" href="{{route('roles.show', ['id' => $role->id])}}">Show</a>
                                                    @permission('role-edit')
                                                    <a class="btn btn-info" href="{{ route('roles.edit', $role->id)}}">Edit</a>
                                                    @endpermission
                                                    @permission('role-delete')
                                                    <a class="btn btn-danger" href="{{ route('roles.destroy', $role->id)}}">Delete</a>
                                                    @endpermission
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-12">
            <div class='pagination-container'>
                {!! $roles->links() !!}
            </div>
        </div>
</div>
@endsection
