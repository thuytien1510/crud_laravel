@extends('layouts.dashboard')

@section('content')
    <div class="card">
        <div class="card-body">
            <div class="card-header">
                <h2>Create New Role</h2>
            </div>
            <div class="col-lg-12 margin-tb">
                <div class="pull-right">
                    <a class="btn btn-primary" href="{{ route('roles.index') }}"> Back</a>
                </div>
            </div>
        </div>
        <div class="container mt-4">
            <form method="POST" action="{{route('roles.store')}}">
                    @csrf
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>Name:</strong>
                            {!! Form::text('name', null, array('placeholder' => 'Name','class' => 'form-control')) !!}
                            @if($errors->has('name'))
                                <div class="error text-danger">{{ $errors->first('name') }}</div>
                            @endif
                        </div>
                        <div class="form-group">
                            <strong>Permission:</strong>
                            @if($errors->has('permission'))
                                <div class="error text-danger">{{ $errors->first('permission') }}</div>
                            @endif
                        </div>
                        @foreach($permissions as $permission)
                            <div class="form-group form-check">
                                <input type="checkbox" class="form-check-input"  value="{{ $permission->id }}" name="permission[]" id="{{ $permission->name }}" >
                                <label for="{{ $permission->name }}" class="form-check-label ">{{ $permission->name }}</label>
                            </div>
                        @endforeach
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </form>
        </div>
    </div>
@endsection

