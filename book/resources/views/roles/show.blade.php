@extends('layouts.dashboard')


@section('content')

    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Show role : {{ $role->name }}</h3>
            <a href="{{ route('roles.index') }}" class="btn btn-primary">Back</a>
        </div>
        <div class="card-body">
            <table id="example2" class="table table-bordered table-hover">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Permission</th>
                </tr>
                </thead>
                <tbody>
                @foreach($role->permissions as $permission)
                    <tr>
                        <td>{{ $permission->id }}</td>
                        <td>{{ $permission->name }}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
