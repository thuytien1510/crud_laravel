<?php

namespace App\Traits;

use Illuminate\Support\Facades\File;
use Intervention\Image\ImageManager;
use Image;

trait HandleImage
{
    protected $path = 'upload/products/';
    protected $imageDefault = 'image_default.png';

    public function verifyImage($request) : bool
    {
        if ($request->file('image')) {
            return true;
        }
        return false;
    }

    public function storeImage($request) :? string
    {
        if ($this->verifyImage($request)) {
            $image = $request->image;
            $imageExt = $image->getClientOriginalExtension();
            $name = time() . '.' . $imageExt;
            $image->move($this->path, $name);
            return $name;
        }
        return $this->imageDefault;
    }

    public function deleteImage($image)
    {
        $path = $this->path . '/' . $image;
        if (file_exists($path) && $image != $this->imageDefault) {
            unlink($path);
        }
    }

    public function updateImage($request, $currentImage)
    {
        if ($this->verifyImage($request)) {
            $this->deleteImage($currentImage);
            return $this->storeImage($request);
        }
        return $currentImage;
    }
}
