<?php
namespace App\Traits;

trait Route
{
    /**======USER=====*/
    public function getListUserRoute()
    {
        return route('users.index');
    }

    public function getCreateUserRoute()
    {
        return route('users.create');
    }

    public function getStoreUserRoute()
    {
        return route('users.store');
    }

    public function getEditUserRoute($id)
    {
        return route('users.edit', $id);
    }

    public function getUpdateUserRoute($id)
    {
        return route('users.update', $id);
    }

    public function getRedirectUserRoute()
    {
        return route('users.index');
    }

    public function getDestroyUserRoute($id)
    {
        return route('users.destroy', $id);
    }

    /**======ROLE=====*/
    public function getListRoleRoute()
    {
        return route('roles.index');
    }

    public function getShowRoleRoute($id)
    {
        return route('roles.show', ['id' => $id]);
    }

    public function getCreateRoleRoute()
    {
        return route('roles.create');
    }

    public function getStoreRoleRoute()
    {
        return route('roles.store');
    }

    public function getEditRoleRoute($id)
    {
        return route('roles.edit', $id);
    }

    public function getUpdateRoleRoute($id)
    {
        return route('roles.update', $id);
    }

    public function getRedirectRoleRoute()
    {
        return route('roles.index');
    }

    public function getDestroyRoleRoute($id)
    {
        return route('roles.destroy', $id);
    }

    /**======CATEGORY=====*/
    public function getListCategoryRoute()
    {
        return route('categories.index');
    }

    public function getShowCategoryRoute($id)
    {
        return route('categories.show', ['id' => $id]);
    }

    public function getCreateCategoryRoute()
    {
        return route('categories.create');
    }

    public function getStoreCategoryRoute()
    {
        return route('categories.store');
    }

    public function getEditCategoryRoute($id)
    {
        return route('categories.edit', $id);
    }

    public function getUpdateCategoryRoute($id)
    {
        return route('categories.update', $id);
    }

    public function getRedirectCategoryRoute()
    {
        return route('categories.index');
    }

    public function getDestroyCategoryRoute($id)
    {
        return route('categories.destroy', $id);
    }

    /**======PRODUCT=====*/
    public function getListProductRoute()
    {
        return route('products.index');
    }

    public function getShowProductRoute($id)
    {
        return route('products.show', ['id' => $id]);
    }

    public function getCreateProductRoute()
    {
        return route('products.create');
    }

    public function getStoreProductRoute()
    {
        return route('products.store');
    }

    public function getEditProductRoute($id)
    {
        return route('products.edit', $id);
    }

    public function getUpdateProductRoute($id)
    {
        return route('products.update', $id);
    }

    public function getRedirectProductRoute()
    {
        return route('products.index');
    }

    public function getDestroyProductRoute($id)
    {
        return route('products.destroy', $id);
    }
}
