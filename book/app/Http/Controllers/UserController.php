<?php

namespace App\Http\Controllers;

use App\Services\RoleService;
use App\Services\UserService;
use Illuminate\Http\Request;
use App\Http\Requests\Users\CreateUserRequest;
use App\Http\Requests\Users\UpdateUserRequest;

class UserController extends Controller
{
    protected $userService;
    protected $roleService;

    public function __construct(UserService $userService, RoleService $roleService)
    {
        $this->userService = $userService;
        $this->roleService = $roleService;
    }

    public function index()
    {
        $users = $this->userService->all();
        return view('users.index', compact('users'));
    }

    public function show($id)
    {
        $user = $this->userService->findOrFail($id);
        return view('users.show', compact('user'));
    }

    public function create()
    {
        $roles = $this->roleService->all();
        return view('users.create', compact('roles'));
    }

    public function store(CreateUserRequest $request)
    {
        $this->userService->create($request);
        return redirect()->route('users.index')->with('message', 'Create User Success !');
    }

    public function edit($id)
    {
        $user = $this->userService->findOrFail($id);
        $roles = $this->roleService->all();
        return view('users.edit', compact('user', 'roles'));
    }

    public function update(UpdateUserRequest $request, $id)
    {
        $this->userService->update($request, $id);
        return redirect()->route('users.index')->with('message', 'Update User Success !');
    }

    public function destroy($id)
    {
        $this->userService->delete($id);
        return redirect()->route('users.index')->with('message', 'Delete User Success !');
    }

    public function search(Request $request)
    {
        $users = $this->userService->search($request);
        $roles = $this->roleService->all();
        return view('users.index', compact('users', 'roles'));
    }
}
