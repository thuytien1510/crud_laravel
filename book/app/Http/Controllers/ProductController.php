<?php

namespace App\Http\Controllers;

use App\Services\CategoryService;
use App\Services\ProductService;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\Products\CreateProductRequest;
use App\Http\Requests\Products\UpdateProductRequest;
use App\Models\Category;
use Image;

class ProductController extends Controller
{
    protected $productService;
    protected $categoryService;

    public function __construct(ProductService $productService, CategoryService $categoryService)
    {
        $this->productService = $productService;
        $this->categoryService = $categoryService;
    }

    public function index(Request $request)
    {
        $products = $this->productService->search($request);
        $category = $this->categoryService->all();
        $categories = $this->categoryService->getParentCategory();
        return view('products.index', compact('products', 'category', 'categories'));
    }

    public function store(CreateProductRequest $request)
    {
        $this->productService->create($request);
        return redirect(route('products.index'));
    }

    public function show(int $id)
    {
        $product = $this->productService->findOrFail($id);
        return redirect(route('products.show', compact('product')));
    }

    public function edit($id)
    {
        $productEdit = $this->productService->findOrFail($id);
        return response()->json(['productEdit' => $productEdit]);
    }

    public function update(UpdateProductRequest $request, $id)
    {
        $this->productService->update($request, $id);
        return redirect(route('products.index'));
    }

    public function destroy($id)
    {
        $this->productService->destroy($id);
        return redirect(route('products.index'));
    }
}
