<?php

namespace App\Http\Controllers;

use App\Http\Requests\Categories\CreateCategoryRequest;
use App\Http\Requests\Categories\UpdateCategoryRequest;
use App\Services\CategoryService;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    protected $categoryService;

    public function __construct(CategoryService $categoryService)
    {
        $this->categoryService = $categoryService;
    }

    public function index()
    {
        $categories = $this->categoryService->paginate();
        return view('categories.index', compact('categories'));
    }

    public function create()
    {
        $categories = $this->categoryService->getParentCategory();
        return view('categories.create', compact('categories'));
    }

    public function store(CreateCategoryRequest $request)
    {
        $this->categoryService->create($request);
        return redirect()->route('categories.index')->with('message', 'Create category Success !');
    }

    public function edit($id)
    {
        $category = $this->categoryService->findOrFail($id);
        $categories = $this->categoryService->getParentCategoryWithout($category->id);
        return view('categories.edit', compact('category', 'categories'));
    }

    public function update(UpdateCategoryRequest $request, $id)
    {
        $this->categoryService->update($request, $id);
        return redirect()->route('categories.index')->with('message', 'Update category Success !');
    }

    public function destroy($id)
    {
        $this->categoryService->delete($id);
        return redirect()->route('categories.index')->with('message', 'Delete category Success !');
    }

    public function search(Request $request)
    {
        $categories = $this->categoryService->search($request);
        return view('categories.index', compact('categories'));
    }
}
