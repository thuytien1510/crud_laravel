<?php

namespace App\Http\Controllers;

use App\Services\RoleService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\Roles\CreateRoleRequest;
use App\Http\Requests\Roles\UpdateRoleRequest;
use App\Models\Permission;
use Illuminate\Support\Str;

class RoleController extends Controller
{
    protected $roleService;

    public function __construct(RoleService $roleService)
    {
        $this->roleService = $roleService;
    }

    public function index()
    {
        $roles = $this->roleService->all();
        return view('roles.index', compact('roles'));
    }

    public function show($id)
    {
        $role = $this->roleService->findOrFail($id);
        return view('roles.show', compact('role'));
    }

    public function create()
    {
        $permissions = Permission::all();
        return view('roles.create', compact('permissions'));
    }

    public function store(CreateRoleRequest $request)
    {
        $this->roleService->create($request);
        return redirect()->route('roles.index')->with('message', 'Create Role Success !');
    }

    public function edit($id)
    {
        $role = $this->roleService->findOrFail($id);
        $permissions = Permission::all();
        return view('roles.edit', compact('role', 'permissions'));
    }

    public function update(UpdateRoleRequest $request, $id)
    {
        $this->roleService->update($request, $id);
        return redirect()->route('roles.index')->with('message', 'Update Role Success !');
    }
    public function destroy($id)
    {
        $this->roleService->delete($id);
        return redirect()->route('roles.index')->with('message', 'Delete Role Success !');
    }

    public function search(Request $request)
    {
        $roles = $this->roleService->search($request);
        return view('roles.index', compact('roles'));
    }
}
