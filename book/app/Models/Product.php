<?php

namespace App\Models;

use App\Traits\HandleImage;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;
    use HandleImage;

    protected $table = 'products';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'price',
        'image',
        'description'
    ];

    /**
     * The categories that belong to the products.
     */
    public function category()
    {
        return $this->belongsToMany(Category::class, 'product_category');
    }

    /**
     * Scope a query to search products by name.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeWithName($query, $name)
    {
        return $name ? $query->where('name', 'LIKE', '%' . $name . '%') : null;
    }

    /**
     * Scope a query to search products by price.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeWithPrice($query, $price)
    {
        return $price ? $query->where('price', '<=', $price) : null;
    }

    /**
     * Scope a query to search products by category.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeWithCategoryName($query, $categoryName)
    {
        return $categoryName ? $query->whereHas('category', fn ($query) =>
        $query->where('name', 'LIKE', '%' . $categoryName . '%')) : null;
    }
}
