<?php

namespace App\Repositories;

use App\Models\Category;

class CategoryRepository extends BaseRepository
{
    /**
     * get model
     * @return string
     */
    public function model()
    {
        return Category::class;
    }

    public function getParentCategory()
    {
        return $this->model->withParentIdNull()->get();
    }

    public function getParentCategoryWithout($id)
    {
        return $this->model->withParentIdNull()->withoutId($id)->get();
    }

    public function search($dataSearch)
    {
        return $this->model->withName($dataSearch['name'])->paginate(5);
    }
}
