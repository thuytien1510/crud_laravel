<?php

namespace App\Repositories;

use App\Models\Role;
use App\Repositories\BaseRepository;

class RoleRepository extends BaseRepository
{
    /**
     * get model
     * @return string
     */
    public function model()
    {
        return Role::class;
    }

    public function get()
    {
        return $this->model->orderBy('id', 'DESC')->paginate(5);
    }

    public function search($dataRole)
    {
        return $this->model->withName($dataRole['name'])->paginate(5);
    }
}
