<?php

namespace App\Repositories;

use App\Models\User;
use App\Repositories\BaseRepository;

class UserRepository extends BaseRepository
{
    public function model()
    {
        return User::class;
    }

    public function get()
    {
        return $this->model->paginate(5);
    }

    public function search($dataSearch)
    {
        return $this->model->withRoleName($dataSearch['role_name'])
            ->withEmail($dataSearch['email'])
            ->withName($dataSearch['name'])->paginate(5);
    }
}
