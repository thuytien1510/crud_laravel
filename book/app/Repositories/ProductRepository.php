<?php

namespace App\Repositories;

use App\Models\Product;

class ProductRepository extends BaseRepository
{
    /**
     * get model
     * @return string
     */
    public function model()
    {
        return Product::class;
    }

    public function all()
    {
        return $this->model->orderBy('id', 'DESC')->paginate(5);
    }

    public function search($dataSearch)
    {
        return $this->model->withName($dataSearch['name'])->withPrice($dataSearch['price'])->withCategoryName($dataSearch['category_name'])
            ->paginate(5);
    }
}
