<?php

namespace App\Services;

use App\Repositories\ProductRepository;
use App\Traits\HandleImage;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use InvalidArgumentException;
use Redirect;
use Response;
use File;
use Image;

class ProductService
{
    use HandleImage;
    /**
     * @var $productRepository
     */
    protected $productRepository;
    /**
     * ProductService constructor.
     *
     * @param ProductRepository $productRepository
     */
    public function __construct(ProductRepository $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    /**
     * Get all product.
     *
     * @return String
     */
    public function all()
    {
        return $this->productRepository->all();
    }

    public function search($request)
    {
        $dataSearch['name'] = $request->name ?? null;
        $dataSearch['category_name'] = $request->category_name ?? null;
        $dataSearch['price'] = $request->price ?? null;
        return $this->productRepository->search($dataSearch);
    }

    public function create($request)
    {
        $dataCreate = $request->all();
        $dataCreate['image'] = $this->storeImage($request);
        $product = $this->productRepository->create($dataCreate);
        if (!empty($request->parent_id)) {
            $product->category()->attach($dataCreate['parent_id']);
        }
        return $product;
    }

    public function findOrFail($id)
    {
        return $this->productRepository->findOrFail($id);
    }

    public function update($request, $id)
    {
        $product = $this->productRepository->findOrFail($id);
        $dataUpdate = $request->all();
        $dataUpdate['category_ids'] = $request->category_id ?? [];
        $dataUpdate['image'] = $this->updateImage($request, $product->image);
        $product = $this->productRepository->update($dataUpdate, $id);
        if (!empty(request()->parent_id)) {
            $product->category()->sync($dataUpdate['parent_id']);
        }
        return $product;
    }

    public function destroy($id)
    {
        $product = $this->productRepository->find($id);
        $product->delete();
        $this->deleteImage($product->image);
        return $product;
    }
}
