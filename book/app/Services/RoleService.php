<?php

namespace App\Services;

use App\Models\Permission;
use App\Models\Role;
use App\Repositories\RoleRepository;
use Illuminate\Support\Facades\Hash;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use InvalidArgumentException;

class RoleService
{
    /**
     * @var $roleRepository
     */
    protected $roleRepository;
    /**
     * RoleService constructor.
     *
     * @param RoleRepository $roleRepository
     */
    public function __construct(RoleRepository $roleRepository)
    {
        $this->roleRepository = $roleRepository;
    }

    /**
     * Get all role.
     *
     * @return String
     */
    public function all()
    {
        return $this->roleRepository->get();
    }

    public function findOrFail($id)
    {
        return $this->roleRepository->findOrFail($id);
    }

    public function create($request)
    {
        $dataCreate = $request->all();
        $role = $this->roleRepository->create($dataCreate);
        if (!empty($request->permission)) {
            $role->permissions()->attach($dataCreate['permission']);
        }
        return $role;
    }

    public function update($request, $id)
    {
        $dataUpdate = $request->all();
        $this->roleRepository->update($dataUpdate, $id);
        $role = $this->roleRepository->findOrFail($id);
        $role->permissions()->sync($dataUpdate['permission']);
        return $role;
    }

    public function delete($id)
    {
        $role = $this->roleRepository->delete($id);
        return $role;
    }

    public function search($request)
    {
        $dataSearch['name'] = $request->name ?? null;
        return $this->roleRepository->search($dataSearch);
    }
}
