<?php

namespace App\Services;

use App\Models\Role;
use App\Models\User;
use App\Repositories\UserRepository;
use Illuminate\Support\Facades\Hash;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use InvalidArgumentException;

class UserService
{
    /**
     * @var $userRepository
     */
    protected $userRepository;
    /**
     * UserService constructor.
     *
     * @param UserRepository $userRepository
     */
    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * Get all user.
     *
     * @return String
     */
    public function all()
    {
        return $this->userRepository->get();
    }

    public function findOrFail($id)
    {
        return $this->userRepository->findOrFail($id);
    }

    public function create($request)
    {
        $data = $request->all();
        $data['password'] = Hash::make($data['password']);
        $user = $this->userRepository->create($data);
        if (!empty(request()->role_id)) {
            $user->roles()->attach($data['role_id']);
        }
        return $user;
    }

    public function update($request, $id)
    {
        $data = $request->all();
        $data['password'] = Hash::make($data['password']);
        $this->userRepository->update($data, $id);
        $user = $this->userRepository->findOrFail($id);
        if (!empty(request()->role_id)) {
            $user->roles()->sync($data['role_id']);
        }
        return $user;
    }

    public function delete($id)
    {
        return $this->userRepository->delete($id);
    }

    public function search($request)
    {
        $dataSearch['name'] = $request->name ?? null;
        $dataSearch['email'] = $request->email ?? null;
        $dataSearch['role_name'] = $request->role_name ?? null;
        return $this->userRepository->search($dataSearch);
    }

    public function register($data)
    {
        $dataCreate = [
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ];
        return $this->userRepository->create($dataCreate);
    }
}
