<?php

namespace Database\Factories;

use App\Models\Permission;
use App\Models\Role;
use Illuminate\Database\Eloquent\Factories\Factory;

class RoleFactory extends Factory
{
    protected $model = Role::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */

    public function definition()
    {
        $permissions = Permission::factory()->create();
        return [
            'name' => strtoupper($this->faker->name()),
            'permission' => $permissions->pluck('id')
        ];
    }
}
