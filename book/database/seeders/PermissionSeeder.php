<?php

namespace Database\Seeders;

use App\Models\Permission;
use App\Models\Role;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions = [
            'role-list',
            'role-create',
            'role-edit',
            'role-delete',
            'products-list',
            'products-create',
            'products-edit',
            'products-delete',
            'user-list',
            'user-create',
            'user-edit',
            'user-delete',
            'category-list',
            'category-create',
            'category-edit',
            'category-delete',
        ];
        DB::table('roles')->insert([
            ['name'=>'SUPER-ADMIN'],
            ['name'=>'ADMIN'],
            ['name'=>'MANAGER'],
        ]);

        foreach ($permissions as $permission) {
            Permission::create(['name' => $permission]);
        }
        $role_id = Role::all()->pluck('id')->toArray();
        $per_id = Permission::all()->pluck('id')->toArray();
        for($i=0;$i<count($role_id);$i++){
            for($j=0;$j<count($per_id);$j++){
                DB::table('permission_role')->insert([
                    ['role_id'=>$role_id[$i],'permission_id'=>$per_id[$j]],
                ]);
            }
        }
    }
}
