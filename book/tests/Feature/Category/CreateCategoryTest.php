<?php

namespace Tests\Feature\Category;

use App\Models\Category;
use App\Traits\Route;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Illuminate\Support\Str;
use Tests\TestCase;

class CreateCategoryTest extends TestCase
{
    use Route;

    /** @test */
    public function unauthenticated_can_not_see_form_create_category()
    {
        $response = $this->get($this->getCreateCategoryRoute());
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('/login');
    }

    /** @test */
    public function authenticated_can_see_form_create_category()
    {
        $this->login(["SUPER-ADMIN"]);
        $response = $this->get($this->getCreateCategoryRoute());
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('categories.create');
    }

    /** @test */
    public function unauthenticatphed_can_not_create_form_category()
    {
        $product = Category::factory()->make()->toArray();
        $response = $this->post($this->getStoreRoleRoute(), $product);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('/login');
    }

    /** @test */
    public function authenticate_can_store_category()
    {
        $this->login(["super-admin"]);
        $data = Category::factory()->make()->toArray();
        $categoryAfter = Category::count();
        $response = $this->post($this->getStoreCategoryRoute(), $data);
        $categoryBefore = Category::count();
        $this->assertEquals($categoryAfter + 1, $categoryBefore);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect($this->getRedirectCategoryRoute());
    }
    /** @test */
    public function authenticate_can_not_store_category_if_name_is_null()
    {
        $this->login(["super-admin"]);
        $data = Category::factory()->make(['name'=>''])->toArray();
        $response = $this->post($this->getStoreCategoryRoute(), $data);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors(['name']);
    }
}
