<?php

namespace Tests\Feature\Category;

use App\Traits\Route;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class ShowCategoryTest extends TestCase
{
    use Route;

    /** @test */
    public function authenticate_can_get_list_category()
    {
        $this->login(["super-admin"]);
        $response = $this->get($this->getListCategoryRoute());
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('categories.index');
    }

    /** @test */
    public function unauthenticated_can_not_get_list_category()
    {
        $response = $this->get($this->getListCategoryRoute());
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('/login');
    }
}
