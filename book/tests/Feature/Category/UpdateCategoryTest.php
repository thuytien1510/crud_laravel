<?php

namespace Tests\Feature\Category;

use App\Models\Category;
use App\Models\Role;
use App\Traits\Route;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class UpdateCategoryTest extends TestCase
{
    use Route;

    /** @test */
    public function unauthenticated_can_not_see_form_edit_category()
    {
        $category = Category::factory()->create();
        $response = $this->get($this->getEditCategoryRoute($category->id));
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('/login');
    }

    /** @test */
    public function authenticated_can_see_form_edit_category()
    {
        $this->login(["super-admin"]);
        $category = Category::factory()->create();
        $response = $this->get($this->getEditCategoryRoute($category->id));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('categories.edit');
        $response->assertSee('Edit Category');
    }

    /** @test */
    public function unauthenticated_can_not_update_form_category()
    {
        $category = Category::factory()->create();
        $dataUpdate = Category::factory()->make()->toArray();
        $response = $this->post($this->getUpdateCategoryRoute($category->id), $dataUpdate);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('/login');
    }

    /** @test */
    public function authenticate_can_update_category()
    {
        $this->login(["super-admin"]);
        $category = Category::factory()->create();
        $dataUpdate = Category::factory()->make()->toArray();
        $response = $this->post($this->getUpdateCategoryRoute($category->id), $dataUpdate);
        $categoryCheck = Category::find($category->id);
        $this->assertSame($categoryCheck->name, $dataUpdate['name']);
        $response->assertRedirect($this->getRedirectCategoryRoute());
        $response->assertStatus(Response::HTTP_FOUND);
    }

    /** @test */
    public function authenticate_can_not_update_category_if_name_is_null()
    {
        $this->login(["super-admin"]);
        $category = Category::factory()->create();
        $dataUpdate = Category::factory()->make(['name'=>''])->toArray();
        $response = $this->post($this->getUpdateCategoryRoute($category->id), $dataUpdate);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors(['name']);
    }
}
