<?php

namespace Tests\Feature\Product;

use App\Models\Product;
use App\Models\Role;
use App\Models\User;
use App\Traits\Route;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Http\Response;
use Tests\TestCase;

class DeleteProductTest extends TestCase
{
    use Route;

    /**
     * @test
     */
    public function authenticate_can_delete_product()
    {
        $this->login(["super-admin"]);
        $Product = Product::factory()->create();
        $countProductBefore = Product::count();
        $response = $this->post($this->getDestroyProductRoute($Product->id));
        $countProductAfter = Product::count();
        $response->assertStatus(Response::HTTP_FOUND);
        $this->assertEquals($countProductBefore - 1, $countProductAfter);
        $this->assertDatabaseMissing('products', $Product->toArray());
    }

    /** @test */
    public function unauthenticate_can_not_delete_product()
    {
        $product = Product::factory()->create();
        $response = $this->get($this->getDestroyRoleRoute($product->id));
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('/login');
    }
}

