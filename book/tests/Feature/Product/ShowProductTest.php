<?php

namespace Tests\Feature\Product;

use App\Models\Product;
use App\Traits\Route;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class ShowProductTest extends TestCase
{
    use Route;

    /** @test */
    public function authenticate_can_get_list_product()
    {
        $this->login(["super-admin"]);
        $response = $this->get($this->getListProductRoute());
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('products.index');
    }

    /** @test */
    public function unauthenticated_can_not_get_list_product()
    {
        $response = $this->get($this->getListProductRoute());
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('/login');
    }
}
