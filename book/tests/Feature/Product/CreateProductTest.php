<?php

namespace Tests\Feature\Product;

use App\Models\Product;
use App\Traits\Route;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Http\UploadedFile;
use Illuminate\Http\Response;
use Tests\TestCase;

class CreateProductTest extends TestCase
{
    use Route;

    /** @test */
    public function unauthenticatphed_can_not_create_form_product()
    {
        $product = Product::factory()->make()->toArray();
        $response = $this->post($this->getStoreRoleRoute(), $product);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('/login');
    }

    /** @test */
    public function authenticate_can_store_product()
    {
        $this->login(["super-admin"]);
        $productBefore = Product::count();
        $dataCreate = Product::factory()
            ->make(['image' => UploadedFile::fake()->image('image_default.png')])
            ->toArray();
        $response = $this->post($this->getStoreProductRoute(), $dataCreate);
        $productAfter = Product::count();
        $this->assertEquals($productBefore + 1, $productAfter);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect($this->getRedirectProductRoute());
    }

    /** @test */
    public function authenticate_can_not_store_product_if_name_null()
    {
        $this->login(["super-admin"]);
        $dataCreate = Product::factory()
            ->make(['image' => UploadedFile::fake()->image('image_default.png'), 'name' => ''])
            ->toArray();
        $response = $this->post($this->getStoreProductRoute(), $dataCreate);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors(['name']);
    }

    /** @test */
    public function authenticate_can_not_store_product_if_price_null()
    {
        $this->login(["super-admin"]);
        $dataCreate = Product::factory()
            ->make(['image' => UploadedFile::fake()->image('image_default.png'), 'price' => ''])
            ->toArray();
        $response = $this->post($this->getStoreProductRoute(), $dataCreate);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors(['price']);
    }

    /** @test */
    public function authenticate_can_not_store_product_if_image_null()
    {
        $this->login(["super-admin"]);
        $dataCreate = Product::factory()->make(['image' => ''])->toArray();
        $response = $this->post($this->getStoreProductRoute(), $dataCreate);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors(['image']);
    }
}
