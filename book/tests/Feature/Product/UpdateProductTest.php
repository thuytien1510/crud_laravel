<?php

namespace Tests\Feature\Product;

use App\Models\Product;
use App\Traits\Route;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Illuminate\Http\UploadedFile;
use Tests\TestCase;

class UpdateProductTest extends TestCase
{
    use Route;

    /** @test */
    public function unauthenticated_can_not_update_form_product()
    {
        $product = Product::factory()->create();
        $dataUpdate = Product::factory()->make()->toArray();
        $response = $this->post($this->getUpdateProductRoute($product->id), $dataUpdate);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('/login');
    }

    /** @test */
    public function authenticate_can_update_product()
    {
        $this->login(["super-admin"]);
        $product = Product::factory()->create();
        $dataUpdate = Product::factory()
            ->make(['image' => UploadedFile::fake()->image('image_default.png')])
            ->toArray();
        $response = $this->post($this->getUpdateProductRoute($product->id), $dataUpdate);
        $productCheck = Product::find($product->id);
        $this->assertSame($productCheck->name, $dataUpdate['name']);
        $this->assertSame($productCheck->description, $dataUpdate['description']);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect($this->getRedirectProductRoute());
    }

    /** @test */
    public function authenticate_can_not_edit_product_if_name_is_null()
    {
        $this->login(["super-admin"]);
        $product = Product::factory()->create();
        $dataUpdate = Product::factory()
            ->make(['image' => UploadedFile::fake()->image('image_default.png'), 'name' => ''])
            ->toArray();
        $response = $this->post($this->getUpdateProductRoute($product->id), $dataUpdate);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors(['name']);
    }

    /** @test */
    public function authenticate_can_not_edit_product_if_price_is_null()
    {
        $this->login(["super-admin"]);
        $product = Product::factory()->create();
        $dataUpdate = Product::factory()
            ->make(['image' => UploadedFile::fake()->image('image_default.png'), 'price' => ''])
            ->toArray();
        $response = $this->post($this->getUpdateProductRoute($product->id), $dataUpdate);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors(['price']);
    }

    /** @test */
    public function authenticate_can_not_edit_product_if_image_is_null()
    {
        $this->login(["super-admin"]);
        $product = Product::factory()->create();
        $dataUpdate = Product::factory()->make(['image' => ''])->toArray();
        $response = $this->post($this->getUpdateProductRoute($product->id), $dataUpdate);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors(['image']);
    }
}
