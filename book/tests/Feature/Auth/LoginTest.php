<?php

namespace Tests\Feature\Auth;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class LoginTest extends TestCase
{
    /** @test */
    public function a_user_can_see_view_login()
    {
        $response = $this->get('/login');
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('auth.login');
        $response->assertSee('Login');
    }

    /** @test */
    public function a_user_can_login()
    {
        $dataLogin = $this->dataValid(['email' => 'dieuthuong1411@gmail.com', 'password' => '12345678']);
        $response = $this->post('/login', $dataLogin);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('/home');
    }

    /** @test */
    public function a_user_can_not_login_if_email_not_valid()
    {
        $dataLogin = $this->dataValid(['email' => 'min@gmail.com']);
        $response = $this->post('/login', $dataLogin);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors('email');
    }

    /** @test */
    public function a_user_can_not_login_if_email_is_null()
    {
        $dataLogin = $this->dataValid(['email' => '']);
        $response = $this->post('/login', $dataLogin);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors('email');
    }

    /** @test */
    public function a_user_can_not_login_if_password_not_valid()
    {
        $dataLogin = $this->dataValid(['password' => '123456789']);
        $response = $this->post('/login', $dataLogin);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors('email');
    }

    /** @test */
    public function a_user_can_not_login_if_password_is_null()
    {
        $dataLogin = $this->dataValid(['password' => '']);
        $response = $this->post('/login', $dataLogin);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors('password');
    }

    public function dataValid($dataLogin = [])
    {
        $data = [
            'email' => $this->faker->unique()->safeEmail(),
            'password' => '12345678'
        ];
        return array_merge($data, $dataLogin);
    }
}
