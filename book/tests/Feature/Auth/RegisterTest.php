<?php

namespace Tests\Feature\Auth;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class RegisterTest extends TestCase
{
    /** @test */
    public function a_user_can_see_view_register()
    {
        $response = $this->get('/register');
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('auth.register');
        $response->assertSee('Register');
    }

    /** @test */
    public function a_user_can__register()
    {
        $dataRegister = $this->dataValid();
        $response = $this->post('/register', $dataRegister);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('/');
    }

    /** @test */
    public function a_user_can_not_register_if_name_is_null()
    {
        $dataRegister = $this->dataValid(['name'=>'']);
        $response = $this->post('/register', $dataRegister);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors('name');
    }

    /** @test */
    public function a_user_can_not_register_if_email_is_null()
    {
        $dataRegister = $this->dataValid(['email'=>'']);
        $response = $this->post('/register', $dataRegister);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors(['email']);
    }

    /** @test */
    public function a_user_can_not_register_if_password_is_null()
    {
        $dataRegister = $this->dataValid(['password'=>'']);
        $response = $this->post('/register', $dataRegister);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors(['password']);
    }

    /** @test */
    public function a_user_can_not_register_if_confirm_is_null()
    {
        $dataRegister = $this->dataValid(['confirm'=>'']);
        $response = $this->post('/register', $dataRegister);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors(['confirm']);
    }

    /** @test */
    public function a_user_can_not_register_if_email_exited()
    {
        $dataRegister = [
            'name' => 'tien',
            'email' => 'admin@gmail.com',
            'password' => '12345678',
            'confirm_password' => '12345678'
        ];
        $response = $this->post('/register', $dataRegister);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors(['email']);
    }

    public function dataValid($dataRegister = [])
    {
        $data = [
            'name' => $this->faker->name(),
            'email' => $this->faker->unique()->safeEmail(),
            'password' => '123456',
            'confirm_password' => '123456'
        ];
        return array_merge($data, $dataRegister);
    }
}
