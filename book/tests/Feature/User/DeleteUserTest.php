<?php

namespace Tests\Feature\User;

use App\Models\User;
use App\Traits\Route;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Http\Response;
use Tests\TestCase;

class DeletetUserTest extends TestCase
{
//    use WithoutMiddleware;
    use Route;

    /** @test*/
    public function unauthenticated_can_not_delete_user()
    {
        $user = User::factory()->create();
        $response = $this->get($this->getDestroyUserRoute($user->id));
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('/login');
    }

    /** @test*/
    public function authenticate_can_destroy_user()
    {
        $this->login(["super-admin"]);
        $user = User::factory()->create();
        $userBefore = User::count();
        $response = $this->get($this->getDestroyUserRoute($user->id));
        $userAfter = User::count();
        $this->assertEquals($userBefore - 1, $userAfter);
        $this->assertDatabaseMissing('users', $user->toArray());
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect($this->getRedirectUserRoute());
    }
}
