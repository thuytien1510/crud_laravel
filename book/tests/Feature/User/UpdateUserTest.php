<?php

namespace Tests\Feature\User;

use App\Models\User;
use App\Traits\Route;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Http\Response;

use Tests\TestCase;

class UpdateUserTest extends TestCase
{
    use Route;

    /** @test */
    public function unauthenticated_can_not_see_edit_form()
    {
        $user = User::factory()->create();
        $response = $this->get($this->getEditUserRoute($user->id));
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('/login');
    }

    /** @test */
    public function authenticated_can_see_edit_form()
    {
        $this->login(["super-admin"]);
        $user = User::factory()->create();
        $response = $this->get($this->getEditUserRoute($user->id));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('users.edit');
        $response->assertSee('Edit User');
    }

    /** @test */
    public function unauthenticated_can_not_update_form_user()
    {
        $user = User::factory()->create();
        $dataUpdate = user::factory()->make()->toArray();
        $response = $this->put($this->getUpdateUserRoute($user->id), $dataUpdate);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('/login');
    }

    /** @test */
    public function authenticate_can_edit_user()
    {
        $this->login(["super-admin"]);
        $user = User::factory()->create();
        $dataUpdate = user::factory()->make()->toArray();

        $data = [
            'name' => $this->faker->name(),
            'email' => $this->faker->unique()->safeEmail(),
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',
        ];
        $response = $this->put($this->getUpdateUserRoute($user->id), $data);
        $userCheck = User::find($user->id);
        $this->assertSame($userCheck->name, $data['name']);
        $this->assertSame($userCheck->email, $data['email']);
        $response->assertRedirect($this->getRedirectUserRoute());
    }

    /** @test */
    public function authenticate_can_not_edit_user_if_name_is_null()
    {
        $this->login(["super-admin"]);
        $user = User::factory()->create();
        $dataUpdate = user::factory()->make(['name' => ''])->toArray();
        $data = [
            'name' => '',
            'email' => $this->faker->unique()->safeEmail(),
            'password' => '123456',
        ];
        $response = $this->put($this->getUpdateUserRoute($user->id), $dataUpdate);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors(['name']);
    }

    /** @test */
    public function authenticate_can_not_edit_user_if_email_is_null()
    {
        $this->login(["super-admin"]);
        $user = User::factory()->create();
        $data = [
            'name' => $this->faker->name(),
            'email' => '',
            'password' => '123456',
        ];
        $response = $this->put($this->getUpdateUserRoute($user->id), $data);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors(['email']);
    }

    /** @test */
    public function authenticate_can_not_edit_user_if_password_is_null()
    {
        $this->login(["super-admin"]);
        $user = User::factory()->create();
        $data = [
            'name' => $this->faker->name(),
            'email' => $this->faker->unique()->safeEmail(),
            'password' => '',
        ];
        $response = $this->put($this->getUpdateUserRoute($user->id), $data);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors(['password']);
    }
}


