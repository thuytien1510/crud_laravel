<?php

namespace Tests\Feature\User;

use App\Models\User;
use App\Traits\Route;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Auth\User as AuthUser;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class CreateUserTest extends TestCase
{
    use Route;

    /** @test */
    public function unauthenticated_can_not_see_create_form()
    {
        $response = $this->get($this->getCreateUserRoute());
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('/login');
    }

    /** @test */
    public function authenticated_can_see_create_form()
    {
        $this->login(["super-admin"]);
        $response = $this->get($this->getCreateUserRoute());
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('users.create');
    }

    /** @test */
    public function unauthenticatphed_can_not_create_form()
    {
        $user = User::factory()->make()->toArray();
        $response = $this->post($this->getStoreUserRoute(), $user);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('/login');
    }

    /** @test */
    public function authenticated_user_can_create_user()
    {
        $this->login(["super-admin"]);
        $userAfter = User::count();
        $data = [
            'name' => $this->faker->name(),
            'email' => $this->faker->unique()->safeEmail(),
            'password' => '123456',
            'confirm_password' => '123456',
        ];
        $response = $this->post($this->getStoreUserRoute(), $data);
        $userBefore = User::count();
        $this->assertEquals($userAfter + 1, $userBefore);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect($this->getRedirectUserRoute());
    }

    /** @test */
    public function authenticated_can_not_create_user_if_name_is_null()
    {
        $this->login(["super-admin"]);
        $user = [
            'name' => '',
            'email' => $this->faker->unique()->safeEmail(),
            'password' => '123456', // password
        ];
        $response = $this->post($this->getStoreUserRoute(), $user);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors('name');
    }

    /** @test */
    public function authenticate_can_not_store_user_if_email_is_null()
    {
        $this->login(["super-admin"]);
        $data = [
            'name' => $this->faker->name(),
            'email' => '',
            'password' => '123456',
        ];
        $response = $this->post($this->getStoreUserRoute(), $data);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors(['email']);
    }

    /** @test */
    public function authenticate_can_not_store_user_if_password_is_null()
    {
        $this->login(["super-admin"]);
        $data = [
            'name' => $this->faker->name(),
            'email' => $this->faker->unique()->safeEmail(),
            'password' => '',
        ];
        $response = $this->post($this->getStoreUserRoute(), $data);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors(['password']);
    }
}
