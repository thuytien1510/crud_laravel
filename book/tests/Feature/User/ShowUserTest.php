<?php

namespace Tests\Feature\User;

use App\Models\User;
use App\Traits\Route;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Http\Response;
use Tests\TestCase;

class ShowUserTest extends TestCase
{
//    use WithoutMiddleware;
    use Route;

    /** @test */
    public function authenticated_can_get_list_user()
    {
        $this->login(["super-admin"]);
        $user = User::factory()->create();
        $response = $this->get($this->getListUserRoute());
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('users.index');
    }

    /** @test */
    public function unauthenticate_can_get_list_user()
    {
        $response = $this->get($this->getListUserRoute());
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('/login');
    }
}
