<?php

namespace Tests\Feature\Role;

use App\Models\Role;
use App\Traits\Route;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class DeleteRoleTest extends TestCase
{
    use Route;

    /** @test*/
    public function unauthenticated_can_not_delete_role()
    {
        $role = Role::factory()->create();
        $response = $this->get($this->getDestroyRoleRoute($role->id));
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('/login');
    }

    /** @test*/
    public function authenticate_can_destroy_role()
    {
        $this->login(["super-admin"]);
        $role = Role::factory()->create();
        $roleBefore = Role::count();
        $response = $this->get($this->getDestroyRoleRoute($role->id));
        $roleAfter = Role::count();
        $this->assertEquals($roleBefore - 1, $roleAfter);
        $this->assertDatabaseMissing('roles', $role->toArray());
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect($this->getRedirectRoleRoute());
    }
}
