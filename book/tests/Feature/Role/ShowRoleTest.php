<?php

namespace Tests\Feature\Role;

use App\Models\Role;
use App\Traits\Route;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class ShowRoleTest extends TestCase
{
    use Route;

    /** @test */
    public function authenticated_can_get_list_role()
    {
        $this->login(["super-admin"]);
        $role = Role::factory()->create();
        $response = $this->get($this->getListRoleRoute());
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('roles.index');
        $response->assertSee($role->name);
    }

    /** @test */
    public function unauthenticate_can_get_list_role()
    {
        $response = $this->get($this->getListRoleRoute());
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('/login');
    }

    /** @test */
    public function authenticated_can_get_role_if_role_exist()
    {
        $this->login(["super-admin"]);
        $role = Role::factory()->create();
        $response = $this->get($this->getShowRoleRoute($role->id));
        $response->assertViewIs('roles.show');
        $response->assertStatus(Response::HTTP_OK);
        $response->assertSee($role->name);
    }
}
