<?php

namespace Tests\Feature\Role;

use App\Models\Permission;
use App\Models\Role;
use App\Traits\Route;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class UpdateRoleTest extends TestCase
{
    use Route;

    /** @test */
    public function unauthenticated_can_not_see_form_edit_role()
    {
        $role = Role::factory()->create();
        $response = $this->get($this->getEditRoleRoute($role->id));
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('/login');
    }

    /** @test */
    public function authenticated_can_see_form_edit_role()
    {
        $this->login(["super-admin"]);
        $role = Role::factory()->create();
        $response = $this->get($this->getEditRoleRoute($role->id));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('roles.edit');
        $response->assertSee('Edit Role');
    }

    /** @test */
    public function unauthenticated_can_not_update_form_role()
    {
        $role = Role::factory()->create();
        $dataUpdate = Role::factory()->make()->toArray();
        $response = $this->post($this->getUpdateRoleRoute($role->id), $dataUpdate);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('/login');
    }

    /** @test */
    public function authenticated_can_edit_role()
    {
        $this->login(["super-admin"]);
        $role = Role::factory()->create();
        $dataUpdate = Role::factory()->make()->toArray();
        $response = $this->post($this->getUpdateRoleRoute($role->id), $dataUpdate);
        $roleName = Role::find($role->id);
        $this->assertSame($roleName->name, $dataUpdate['name']);
        $this->assertDatabaseHas('roles', ['name' => $dataUpdate['name']]);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect($this->getRedirectRoleRoute());
    }

    /** @test */
    public function authenticate_can_not_edit_role_if_name_is_null()
    {
        $this->login(["super-admin"]);
        $role = Role::factory()->create();
        $dataUpdate = Role::factory()->make(['name' => ''])->toArray();
        $response = $this->post($this->getUpdateRoleRoute($role->id), $dataUpdate);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors(['name']);
    }

    /** @test */
    public function authenticate_can_not_edit_role_if_permission_is_null()
    {
        $this->login(["super-admin"]);
        $role = Role::factory()->create();
        $dataUpdate = Role::factory()->make(['permission' => ''])->toArray();
        $response = $this->post($this->getUpdateRoleRoute($role->id), $dataUpdate);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors(['permission']);
    }
}
