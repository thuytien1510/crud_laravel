<?php

namespace Tests\Feature\Role;

use App\Models\Permission;
use App\Models\Role;
use App\Traits\Route;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class CreateRoleTest extends TestCase
{
    use Route;

    /** @test */
    public function unauthenticated_can_not_see_form_create_role()
    {
        $response = $this->get($this->getCreateRoleRoute());
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('/login');
    }

    /** @test */
    public function authenticated_can_see_form_create_role()
    {
        $this->login(["super-admin"]);
        $response = $this->get($this->getCreateRoleRoute());
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('roles.create');
    }

    /** @test */
    public function unauthenticated_can_not_create_form_role()
    {
        $role = Role::factory()->make()->toArray();
        $response = $this->post($this->getStoreRoleRoute(), $role);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('/login');
    }

    /** @test */
    public function authenticated_user_can_create_role()
    {
        $this->login(["super-admin"]);
        $roleAfter = Role::count();
        $dataCreate = Role::factory()->make()->toArray();
        $response = $this->post($this->getStoreRoleRoute(), $dataCreate);
        $roleBefore = Role::count();
        $this->assertEquals($roleAfter + 1, $roleBefore);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect($this->getRedirectRoleRoute());
    }

    /** @test */
    public function authenticated_can_not_create_role_if_name_is_null()
    {
        $this->login(["super-admin"]);
        $dataCreate = Role::factory()->make(['name' => ''])->toArray();
        $response = $this->post($this->getStoreRoleRoute(), $dataCreate);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors('name');
    }

    /** @test */
    public function authenticate_can_not_store_role_if_permission_is_null()
    {
        $this->login(["super-admin"]);
        $dataCreate = Role::factory()->make(['permission' => ''])->toArray();
        $response = $this->post($this->getStoreRoleRoute(), $dataCreate);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors(['permission']);
    }

    /** @test */
    public function authenticate_can_not_store_role_if_data_is_null()
    {
        $this->login(["super-admin"]);
        $dataCreate = Role::factory()->make(['name' => '', 'permission' => ''])->toArray();
        $response = $this->post($this->getStoreRoleRoute(), $dataCreate);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors(['name', 'permission']);
    }
}
